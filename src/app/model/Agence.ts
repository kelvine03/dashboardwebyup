export class Agence{
    id:string;
    nom:string;
    telephone:string;
    ville:string;
    longitude:string;
    latitude:string;
    situation_geographique:string;
    image:string;
    statut:string;
    category_id:string;


}