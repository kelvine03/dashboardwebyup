import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})



export class LoginComponent implements OnInit{ 


 constructor(private router:Router){}

 ngOnInit(){}


 login(e){
   e.preventDefault();

   const target = e.target; 
   const email = target.querySelector('#email').value;
   const password = target.querySelector('#password').value;
   if(email === password){
    this.router.navigate(['/dashboard']);
   }
 }

}
