import { Component, OnInit} from '@angular/core';

@Component({
  templateUrl: 'add-user.component.html',
  styleUrls: ['add-user.component.scss']
})
export class AddUserComponent implements OnInit{


  constructor() { }


  ngOnInit(){
    
  }

  // collapse config
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }
  // end of collapse config

}
