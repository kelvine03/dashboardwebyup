import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from "./ajouter/add-user.component";

import { UserListComponent } from "./list/user-list.component";




const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Utilisateurs'
      },
      children: [
        {
          path: '',
          redirectTo: 'add'
        },
   
        {
          path: 'add',
          component: AddUserComponent,
          data: {
            title: 'Ajouter'
          }
        },

        {
          path: 'list',
          component: UserListComponent,
          data: {
            title: 'liste des utilisateurs'
          }
        },
  
  
      ]
    }
  ];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
