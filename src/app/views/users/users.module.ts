// Angular
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';





// Collapse Component
import { CollapseModule } from 'ngx-bootstrap/collapse';


// Dropdowns Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Components Routing
import { UsersRoutingModule } from './users-routing.module';


import { AddUserComponent } from "./ajouter/add-user.component";

import { UserListComponent } from "./list/user-list.component";
import { ModifierComponent } from './modifier/modifier.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UsersRoutingModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),

  ],
  declarations: [
    AddUserComponent,
    UserListComponent,
    ModifierComponent
  ]
})
export class UsersModule { }
