import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAgenceComponent } from './ajouter/add-agence.component';
import { ListAgenceComponent } from './list/list-agence.component';

import { ImportAgenceComponent } from "./importer/import-agence.component";

import { MapAgenceComponent } from './map/map-agence.component';

import { ModifierAgenceComponent } from "./modifier/modifier-agence.component";



const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Agences'
    },
    children: [
      {
        path: '',
        redirectTo: 'list'
      },
 
      {
        path: 'add',
        component: AddAgenceComponent,
        data: {
          title: 'Ajouter'
        }
      },

      {
        path: 'import',
        component: ImportAgenceComponent,
        data: {
          title: 'Importer'
        }
      },

      {
        path: 'modifier/:id',
        component: ModifierAgenceComponent,
        data: {
          title: 'Modification'
        }
      },

     
      {
        path: 'list',
        component: ListAgenceComponent,
        data: {
          title: 'List'
        }
      },

      {
        path: 'carte',
        component: MapAgenceComponent,
        data: {
          title: 'Map'
        }
      },


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgencesRoutingModule {}
