import { Component ,OnInit} from '@angular/core';
import { AgenceService } from "../../../service/agence.service";
import { CategorieService } from "../../../service/categorie.service";
import { Agence } from "../../../model/Agence";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
declare var $:any;


@Component({
  templateUrl: 'add-agence.component.html',
  styleUrls: ['add-agence.component.scss']
})

export class AddAgenceComponent implements OnInit{

  categories:any;


  constructor(private _agenceService:AgenceService,private _categorieService:CategorieService,  
    private toastrService:ToastrService,
    private router:Router) {

   }
   


  ngOnInit(){ 
    this.getAllCategories();
  }

  // FUNCTION TO GET ALL Categories
  getAllCategories(){
    this._categorieService.showAllCategories()
      .subscribe(categories => this.categories = categories
        );
    }

   // SUCCESS MESSAGE 
   successMessage(){
    this.toastrService.success('L\'agence a été ajoutée avec success', 'MESSAGE DE SUCCES');
  }

  //ERROR MESSAGE
  errorMessage(){
    this.toastrService.error('L\'ajout de l\'agence a echouée, veuillez réessayer!', 'MESSAGE D\'ERREUR');
  }

  //******************** */
  model = new Agence();

 // config to display file selected on image place
 imageUploaded(file: any){
  $('img').hide();
}

imageRemoved(file: any){
  $('img').show();

}
// End config to display file selected on image place



   // CANCEL REDIRECT ROUT
   goBack(){
    this.router.navigate(['/agences/list']);
  }


  //ADDING CATEGORIE IN DATABASE
  addAgence(){
    this._agenceService
    .addAgence(this.model)
    .subscribe(response =>{
      //response to display in succes insertion
      console.log(response);
      this.router.navigate(['/agences/list']);
      this.successMessage();
    })
  }




  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';
  collapsed(event: any): void {
    // console.log(event);
  }
  expanded(event: any): void {
    // console.log(event);
  }
  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

}
