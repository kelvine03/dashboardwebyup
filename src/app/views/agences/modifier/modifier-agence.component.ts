import { Component ,OnInit} from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";

//MODEL
import { Agence } from "../../../model/Agence";

//SERVICE
import { AgenceService } from "../../../service/agence.service";
import { CategorieService } from "../../../service/categorie.service";

// TOASTR
import { ToastrService } from 'ngx-toastr';



declare var $:any;


@Component({
  templateUrl: 'modifier-agence.component.html',
  styleUrls: ['modifier-agence.component.scss']
})

export class ModifierAgenceComponent implements OnInit{
  
  categories:any;
  agence:Agence;
  nom:string = '';

  
  constructor(
    private _agenceService:AgenceService,
    private _categorieService:CategorieService,
    private route:ActivatedRoute,
    private router:Router,
    private toastrService:ToastrService,) { }


  ngOnInit(){ 
    this.getOneAgence();
    this.getAllCategories(); }

    // FUNCTION TO GET ALL Categories
  getAllCategories(){
    this._categorieService.showAllCategories()
      .subscribe(categories => this.categories = categories
        );
    }

  // config to display file selected on image place
  imageUploaded(file: any){
    $('img').hide();
    //$('#btn-save').removeAttr('disabled');
  }

  imageRemoved(file: any){
    $('img').show();
    
  }
  // End config to display file selected on image place

  // SUCCESS MESSAGE 
  successMessage(){
    this.toastrService.success('L\'agence a été modifiée avec succes', 'MESSAGE DE SUCCES');
  }

  //ERROR MESSAGE
  errorMessage(){
    this.toastrService.error('La modification de l\'agence a echouée', 'MESSAGE D\'ERRUR');
  }

  // CANCEL REDIRECT ROUTE
  goBack(){
    this.router.navigate(['/agences/list']);
  }


  //******************** */
  model = new Agence();

  getOneAgence(){
    var id = this.route.snapshot.params['id'];

    this._agenceService.showAgence(id)
      .subscribe(
        agence => {
          this.agence = agence[0];
          this.agence.id=id;

        }
        );
    
  }

  updateAgence(){
    this._agenceService
    .editAgence(this.agence)
    .subscribe(response=>{
      console.log(response);
      this.router.navigate(['/agences/list']);
      this.successMessage()
    });
  }


  

}
