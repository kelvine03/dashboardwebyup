// Angular
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
// Components Routing
import { AgencesRoutingModule } from './agences-routing.module';
import { ImageUploadModule } from "angular2-image-upload";

// SERVICE******
import { AgenceService } from "../../service/agence.service";
import { CategorieService } from "../../service/categorie.service";




// agence Component
import { AddAgenceComponent } from './ajouter/add-agence.component';

import { ImportAgenceComponent } from './importer/import-agence.component';

import { ListAgenceComponent } from './list/list-agence.component';

import { MapAgenceComponent } from "./map/map-agence.component";

import { ModifierAgenceComponent } from "./modifier/modifier-agence.component";



// Collapse Component
import { CollapseModule } from 'ngx-bootstrap/collapse';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AgencesRoutingModule,
    CollapseModule.forRoot(),
    ImageUploadModule.forRoot(),
    HttpModule,
    DataTablesModule

  ],
  declarations: [
    AddAgenceComponent,
    ListAgenceComponent,
    MapAgenceComponent,
    ImportAgenceComponent,
    ModifierAgenceComponent

  ],
  providers:[AgenceService,CategorieService]
})
export class AgencesModule { }
