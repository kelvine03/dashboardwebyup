import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  templateUrl: 'map-agence.component.html',
  styleUrls:['map-agence.component.scss']
})
export class MapAgenceComponent implements OnInit{

  constructor() { }


  ngOnInit() {
    // Déclaration de la carte avec les coordonnées du centre et le niveau de zoom.
    const myfrugalmap = L.map('frugalmap').setView([5.4285, -4.0059], 12);
   
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'Frugal Map'
    }).addTo(myfrugalmap);

    const myIcon1 = L.icon({
      iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
    });
    L.marker([5.2671, -3.9214], {icon: myIcon1}).bindPopup('Nom: Soro distribution').addTo(myfrugalmap).openPopup();
    const myIcon2 = L.icon({
      iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
    });
    L.marker([5.2944, -3.9786], {icon: myIcon2}).bindPopup('Nom: Songon distribution').addTo(myfrugalmap).openPopup();
    const myIcon3 = L.icon({
      iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
    });
    L.marker([5.4034, -3.9854], {icon: myIcon3}).bindPopup('Nom: Roman distribution').addTo(myfrugalmap).openPopup();
   
  }
  
  


}
