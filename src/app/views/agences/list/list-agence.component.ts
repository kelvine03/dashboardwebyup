import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params, Router } from "@angular/router";

// model importing
import { Agence } from "../../../model/Agence";


// service importing
import { AgenceService } from "../../../service/agence.service";
import { CategorieService } from "../../../service/categorie.service";

// TOASTR
import { ToastrService } from 'ngx-toastr';
import { Categorie } from '../../../model/Categorie';


declare var $;

@Component({
  templateUrl: 'list-agence.component.html'
})
export class ListAgenceComponent implements OnInit{

  agences:any;
  nomCategorie:string;
  categories:any
  categorie:Categorie

    // DATATABLE CONFIG
    dtOptions: DataTables.Settings = {};
 
    // END OF DATATABLE COONFIG
  
    constructor(
      private _agenceService:AgenceService,
      private _categorieService:CategorieService, 
      private _router:Router,
      private toastrService:ToastrService) { }
  
    ngOnInit() {
      this.getAllAgences();
      this.dtOptions = {
      };
    }

      // SUCCESS MESSAGE 
  successMessage(){
    this.toastrService.success('Agence supprimée avec success', 'MESSAGE DE SUCCES');
  }

  //ERROR MESSAGE
  errorMessage(){
    this.toastrService.error('La suppression a été annulée', 'MESSAGE D\'ERREUR');
  }


  // FUNCTION TO GET ALL Categories
  getAllAgences(){
    this._agenceService.showAllAgences()
      .subscribe(agences => this.agences = agences
        );
    }


    //DELETING CATEGORIE
    deleteAgence(id){
      var response = confirm("Etes-vous sur de vouloir vraiment supprimer ce message?");
       if(response == true){
        this._agenceService
        .deleteAgence(id)
        .subscribe(response => {
          console.log(response);
          this.getAllAgences();
          this.successMessage()
        })
       }else{
        this.errorMessage()
       }
    }




  


}
