import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";

//MODEL
import { Categorie } from "../../../model/Categorie";

//SERVICE
import { CategorieService } from "../../../service/categorie.service";

// TOASTR
import { ToastrService } from 'ngx-toastr';



declare var $:any;

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.scss']
})
export class ModifierComponent implements OnInit {

  categorie:Categorie; 
  nom:string = '';

  constructor(private _categorieService:CategorieService,
    private route:ActivatedRoute,
    private router:Router,
    private toastrService:ToastrService,) { }


    public file_src:string;

  ngOnInit() {
    this.getOneCategorie();
  }

  
  // config to display file selected on image place
  imageUploaded(file: any){
    $('img').hide();
    //$('#btn-save').removeAttr('disabled');
  }

  imageRemoved(file: any){
    $('img').show();
    
  }
  // End config to display file selected on image place


  // SUCCESS MESSAGE 
  successMessage(){
    this.toastrService.success('La catégorie a été modifiée avec succes', 'MESSAGE DE SUCCES');
  }

  //ERROR MESSAGE
  errorMessage(){
    this.toastrService.error('La modification de la catégorie a echouée', 'MESSAGE D\'ERRUR');
  }

  // CANCEL REDIRECT ROUTE
  goBack(){
    this.router.navigate(['/categories/list']);
  }


  //******************** */
  model = new Categorie();

  getOneCategorie(){
    var id = this.route.snapshot.params['id'];

    this._categorieService.showCategorie(id)
      .subscribe(
        categorie => {
          this.categorie = categorie[0];
          this.categorie.id=id;

        }
        );
    
  }

  updateCategorie(){
    this._categorieService
    .editCategorie(this.categorie)
    .subscribe(response=>{
      console.log(response);
      this.router.navigate(['/categories/list']);
      this.successMessage()
    });
  }



}
