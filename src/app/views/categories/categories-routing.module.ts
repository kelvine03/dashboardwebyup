import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCategorieComponent } from "./ajouter/add-categorie.component";
import { ListCategorieComponent } from "./list/list-categorie.component";
import { ModifierComponent } from "./modifier/modifier.component";


const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Categories d agence'
      },
      children: [
        {
          path: '',
          redirectTo: 'list'
        },
        {
          path: 'add',
          component: AddCategorieComponent,
          data: {
            title: 'Ajouter'
          }
        },
        {
          path: 'list',
          component: ListCategorieComponent,
          data: {
            title: 'List'
          }
        },
        {
          path: 'modifier/:id',
          component: ModifierComponent,
          data: {
            title: 'Modification'
          }
        },
  
  
      ]
    }
  ];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule {}
