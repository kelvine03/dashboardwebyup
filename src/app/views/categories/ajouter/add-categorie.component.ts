import { Component ,OnInit} from '@angular/core';
import { Categorie } from "../../../model/Categorie";
import { CategorieService } from "../../../service/categorie.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

declare var $:any;

@Component({
  templateUrl: 'add-categorie.component.html',
  styleUrls: ['add-categorie.component.scss']
})
export class AddCategorieComponent implements OnInit{


  constructor(
    private _categoriService:CategorieService, 
    private toastrService:ToastrService,
    private router:Router) { }


    ngOnInit(){
      
    }

    // SUCCESS MESSAGE 
    successMessage(){
      this.toastrService.success('La Catégorie a été ajoutée avec success', 'MESSAGE DE SUCCES');
    }

    //ERROR MESSAGE
    errorMessage(){
      this.toastrService.error('L\'ajout de la catégorie a echouée, veuillez réessayer!', 'MESSAGE D\'ERREUR');
    }

    //******************** */
    model = new Categorie();

    // config to display file selected on image place
  imageUploaded(file: any){
    $('img').hide();
  }

  imageRemoved(file: any){
    $('img').show();

  }
  // End config to display file selected on image place


   // CANCEL REDIRECT ROUT
   goBack(){
    this.router.navigate(['/categories/list']);
  }


  //ADDING CATEGORIE IN DATABASE
  addCategorie(){
    this._categoriService
    .addCategorie(this.model)
    .subscribe(response =>{
      //response to display in succes insertion
      console.log(response);
      this.router.navigate(['/categories/list']);
      this.successMessage();
    })
  }


    // COLLAPSE OPTION CONFIG
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';
  collapsed(event: any): void {
    // console.log(event);
  }
  expanded(event: any): void {
    // console.log(event);
  }
  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }


 



}