// Angular
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ImageUploadModule } from "angular2-image-upload";
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';



// Components Routing
import { CategoriesRoutingModule } from './categories-routing.module';
import { AddCategorieComponent } from "./ajouter/add-categorie.component";
import { ListCategorieComponent } from './list/list-categorie.component';

// SERVICES
import { CategorieService } from "../../service/categorie.service";
import { ModifierComponent } from './modifier/modifier.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CategoriesRoutingModule,
    ImageUploadModule.forRoot(),
    HttpModule,
    DataTablesModule
      
  ],
  declarations: [
    AddCategorieComponent,
    ListCategorieComponent,
    ModifierComponent

  ],
  providers:[CategorieService]
})
export class CategoriesModule { }
