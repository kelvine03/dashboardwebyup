import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params, Router } from "@angular/router";

// model importing
import { Categorie } from "../../../model/Categorie";

// service importing
import { CategorieService } from "../../../service/categorie.service";

// TOASTR
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-categorie',
  templateUrl: './list-categorie.component.html',
  styleUrls: ['./list-categorie.component.scss']
})
export class ListCategorieComponent implements OnInit {

  categories: any;


  // DATATABLE CONFIG
  dtOptions: DataTables.Settings = {};
 
  // END OF DATATABLE COONFIG

  constructor(
    private _categorieService:CategorieService, 
    private _router:Router,
    private toastrService:ToastrService) { }

  ngOnInit() {
    this.getAllCategories();
    this.dtOptions = {
    };
  }

  // SUCCESS MESSAGE 
  successMessage(){
    this.toastrService.success('Categorie supprimée avec success', 'MESSAGE DE SUCCES');
  }

  //ERROR MESSAGE
  errorMessage(){
    this.toastrService.error('La suppression a été annulée', 'MESSAGE D\'ERREUR');
  }

  // FUNCTION TO GET ALL Categories
  getAllCategories(){
    this._categorieService.showAllCategories()
      .subscribe(categories => this.categories = categories
        );
    }


    //DELETING CATEGORIE
    deleteCategorie(id){
      var response = confirm("Etes-vous sur de vouloir vraiment supprimer ce message?");
       if(response == true){
        this._categorieService
        .deleteCategorie(id)
        .subscribe(response => {
          console.log(response);
          this.getAllCategories();
          this.successMessage()
        })
       }else{
        this.errorMessage()
       }
    }

}
