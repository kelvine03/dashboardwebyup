interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    title: true,
    name: 'Accueil',
  },
  {
    name: 'Tableau de bord',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    title: true,
    name: 'Composants'
  },


  {
    name: 'Agences',
    url: '/agences',
    icon: 'icon-puzzle',
    children: [

      {
        name: 'Liste des agences',
        url: '/agences/list',
        icon: 'icon-puzzle'
      },
      {
        name: 'Voire sur la carte',
        url: '/agences/carte',
        icon: 'icon-puzzle'
      },

    ]
  },
 

  // users router start here
  {
    name: 'Utilisateurs',
    url: '/users/list',
    icon: 'icon-puzzle',
  },

  // categories router start here
  {
    name: 'Catégories d\'agence ',
    url: '/categories/list',
    icon: 'icon-puzzle',
   
  },

  
  
];
