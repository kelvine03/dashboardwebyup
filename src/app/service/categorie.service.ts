import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';



@Injectable({
  providedIn: 'root'
})
export class CategorieService {

   // local LARAVEL server
   server = 'http://localhost/yupbackend/public/'

   headers: Headers = new Headers;
   options:any;

  constructor(private http:Http) { 
    // Headers config***********
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');
    this.options = new RequestOptions({headers:this.headers});
  }

  // ADDING CATEGORIE 
  addCategorie(info){
    var data = JSON.stringify(info);
    return this.http.post(this.server+"addcategorie",data,this.options).map(
      res => res.json()
    );
  }

  // GETTING CATEGORIES
  showAllCategories(){
    return this.http.get(this.server+"categories").map(
      res => res.json());
  }

  //GETTING ON CATEGORIE BY ID
  showCategorie(id){
    return this.http.get(this.server+"select_categorie/"+id).map(
      res => res.json());
  }


  // UPDATING CATEGORIE 
  editCategorie(info){
    var data = JSON.stringify(info);
    return this.http.post(this.server+"editcategorie",data,this.options).map(
      res => res.json()
    );

  }

  // UPDATING CATEGORIE 
  deleteCategorie(id){
    var data = JSON.stringify(id);
    return this.http.get(this.server+"delete_categorie/"+id).map(
      res => res.json()
    );

  }

  
}



