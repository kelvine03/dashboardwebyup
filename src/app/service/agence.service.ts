import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class AgenceService {

   // local LARAVEL server
   server = 'http://localhost/yupbackend/public/'

   headers: Headers = new Headers;
   options:any;

  constructor(private http:Http) { 
        // Headers config***********
    this.headers.append('enctype','multipart/form-data');
    this.headers.append('Content-Type','application/json');
    this.headers.append('X-Requested-With','XMLHttpRequest');
    this.options = new RequestOptions({headers:this.headers});
  }

  // ADDING AGENCE 
  addAgence(info){
    var data = JSON.stringify(info);
    return this.http.post(this.server+"addagence",data,this.options).map(
      res => res.json()
    );
  }

  // GETTING AGENCE
  showAllAgences(){
    return this.http.get(this.server+"agences").map(
      res => res.json());
  }

  //GETTING ON AGENCE BY ID
  showAgence(id){
    return this.http.get(this.server+"select_agence/"+id).map(
      res => res.json());
  }


  // UPDATING AGENCE 
  editAgence(info){
    var data = JSON.stringify(info);
    return this.http.post(this.server+"editagence",data,this.options).map(
      res => res.json()
    );

  }

  // UPDATING AGENCE 
  deleteAgence(id){
    var data = JSON.stringify(id);
    return this.http.get(this.server+"delete_agence/"+id).map(
      res => res.json()
    );

  }


    
}
